# Kustomize service deployment

Create a new branch for each new environment where this is deployed, with your own matching overlay. Or just rename the my-branch one, ``git branch -m my-branch amazing-staging``.
